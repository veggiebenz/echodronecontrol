Go {forward | Direction} {ten | Distance} {feet | Unit}
Go {backward | Direction} {three | Distance} {yards | Unit}
Go {up | Direction} {twenty five | Distance} {meters | Unit}
Go {down | Direction} {one | Distance} {foot | Unit}
Go {left | Direction} {fifteen | Distance} {yard | Unit}
Go {right | Direction} {two | Distance} {meter | Unit}
Go {back | Direction} {four | Distance} {feet | Unit}
Go {straight | Direction} {five | Distance} {feet | Unit}
Command {Land | Task} 
Command {Return to Launch | Task}
Command {R.T.L. | Task}
Command {Stay | Task}
Command {Hold | Task}
Turn {right|Direction} {ninety | Rotation} degrees
Turn {left | Direction} {forty five | Rotation} degrees
Turn {left|Direction} 




